# Copyright 2017 Tecnativa - Jairo Llopis
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
{
    "name": "Website Form Builder",
    "summary": "Build customized forms in your website",
    "version": "2.0",
    "category": "Website",
    "website": "https://gitlab.com/flectra-community/website",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "license": "LGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "website_form",
    ],
    "data": [
        "templates/assets.xml",
        "templates/snippets.xml",
        "views/ir_model.xml",
    ],
}
