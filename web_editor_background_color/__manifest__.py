# Copyright 2017 Jairo Llopis <jairo.llopis@tecnativa.com>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).
{
    "name": "Web Editor Background Color Picker",
    "summary": "Set any background color for web editor snippets",
    "version": "0.0",
    "category": "Website",
    "website": "https://gitlab.com/flectra-community/web",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "license": "LGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "web_editor",
    ],
    "data": [
        "templates/assets.xml",
    ],
}
