# -*- coding: utf-8 -*-
{
    'name': 'Base (Other)',
    'summary': "Extension of Base for 'Other'-Moduls",

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Administration',
    'version': '0.1',

    'depends': [
        'base',
    ],

    'auto_install': True,
}
