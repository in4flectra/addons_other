# Copyright 2017-2018 Ivan Yelizariev <https://it-projects.info/team/yelizariev>
# Copyright 2017-2019 Kolushov Alexandr <https://it-projects.info/team/KolushovAlexandr>
# Copyright 2017 Ilmir Karamov <https://it-projects.info/team/ilmir-k>
# Copyright 2017-2018 Dinar Gabbasov <https://it-projects.info/team/GabbasovDinar>
# License MIT (https://opensource.org/licenses/MIT).
{
    "name": """Sync POS orders across multiple sessions""",
    "summary": """Use multiple POS for handling orders""",
    "category": "Point of Sale",
    "version": "2.10",
    "application": False,
    "author": "IT-Projects LLC, Ivan Yelizariev",
    "website": "https://yelizariev.github.io",
    "license": "Other OSI approved licence",  # MIT
    "depends": ["pos_disable_payment", "pos_multi_session_sync"],
    "data": [
        "data/pos_multi_session_data.xml",
        "security/ir.model.access.csv",
        "views/pos_multi_session_views.xml",
        "views/multi_session_view.xml",
    ],
    "qweb": ["static/src/xml/pos_multi_session.xml"],
    "post_load": None,
    "pre_init_hook": None,
    "post_init_hook": None,
    "auto_install": False,
    "installable": True,
}
