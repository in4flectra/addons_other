{
    "name": """POS: Longpolling support""",
    "summary": """Technical module to implement instant updates in POS""",
    "category": "Point of Sale",
    "version": "2.0",
    "application": False,
    "author": "IT-Projects LLC, Dinar Gabbasov",
    "website": "https://twitter.com/gabbasov_dinar",
    "license": "Other OSI approved licence",  # MIT
    "depends": ["bus", "point_of_sale"],
    "data": ["views/pos_longpolling_template.xml", "views/pos_longpolling_view.xml"],
    "qweb": ["static/src/xml/pos_longpolling_connection.xml"],
    "post_load": None,
    "pre_init_hook": None,
    "post_init_hook": None,
    "auto_install": False,
    "installable": True,
}
