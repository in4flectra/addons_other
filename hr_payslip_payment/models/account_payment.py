# -*- coding: utf-8 -*-

from flectra import api, models, fields, _


class AccountPayment(models.Model):

    _inherit = 'account.payment'

    payslip_id = fields.Many2one('hr.payslip', string='Pay Slip', copy=False, help='Pay Slip where the payment come from')

    @api.onchange('partner_type')
    def _onchange_partner_type(self):
        if self.partner_type == 'customer':
            return {'domain': {'partner_id': [('customer', '=', True)]}}
        elif self.partner_type == 'supplier':
            return {'domain': {'partner_id': ['|', ('supplier', '=', True), ('employee', '=', True)]}}

    @api.one
    @api.depends('invoice_ids', 'payment_type', 'partner_type', 'partner_id')
    def _compute_destination_account_id(self):
        if self.payslip_id:
            self.destination_account_id = self.payslip_id.journal_id.default_credit_account_id.id
        else:
            super(AccountPayment, self)._compute_destination_account_id()

    def _get_counterpart_move_line_vals(self, invoice=False):
        result = super(AccountPayment, self)._get_counterpart_move_line_vals(invoice)
        if self.payslip_id:
            result['name'] = _('Employee Payment') + ': ' + self.payslip_id.number
            result['ref'] = self.payslip_id.number
        return result

    @api.model
    def get_sequence_code(self):
        if self.payslip_id:
            result = 'account.payment.salary.slip'
        else:
            result = super(AccountPayment, self).get_sequence_code()
        return result
