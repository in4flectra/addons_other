# -*- coding: utf-8 -*-
{
    'name': 'Payslip Payment',
    'version': '1.0',
    'category': 'Human Resources',

    'license': 'AGPL-3',
    'website':'https://www.inteslar.com',

    'author':'inteslar',
    'summary': 'Payslip Payment By Inteslar',
    'description': """
Key Features
------------
* Batchwise Payslips Register Payment
        """,
    'depends': ['base',
                'hr',
                'account',
                'hr_payroll_account',
                'hr_payroll',
                ],
    'data': [
        # wizards
        'wizard/hr_payroll_register_payment.xml',
        'wizard/hr_payroll_batchwise_register_payment.xml',

        # views
        'views/account_payment_views.xml',
        'views/hr_payslip_views.xml',

        # data
        'data/account_data.xml',
    ],
    'installable': True,
    'auto_install': False,
}
