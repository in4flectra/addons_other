# -*- coding: utf-8 -*-

import collections
import datetime
import logging
import zipfile

from io import BytesIO

from flectra import api, models, _
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT

_logger = logging.getLogger(__name__)


class AccountBankStatementImport(models.TransientModel):
    """Add process_camt method to account.bank.statement.import."""
    _inherit = 'account.bank.statement.import'

    ####
    # Change name to reflect that this statement was created importing a CAMT file
    @api.model
    def _create_bank_statements(self, stmt_vals):
        statement_ids, notifs = super(AccountBankStatementImport,self)._create_bank_statements(stmt_vals)
        lang_id = self.env['res.lang']._lang_get(self.env.context.get('lang') or 'en_US')
        for statement_id in statement_ids:
            statement = self.env['account.bank.statement'].browse(statement_id)
            statement_date = datetime.datetime.strptime(statement.date, DEFAULT_SERVER_DATE_FORMAT)
            statement_date = datetime.datetime.strftime(statement_date, lang_id.date_format)
            name = _('%(date)s CAMT %(name)s') % {'date': statement_date, 'name': statement.journal_id.name}
            statement.write({'name': name})

        return statement_ids, notifs

    @api.model
    def _parse_file(self, data_file):
        """Parse a CAMT053 XML file."""
        try:
            parser = self.env['account.bank.statement.import.camt.parser']
            currency, account_number, statements = parser.parse(data_file)
            statements = self._normalize(statements)
            return currency, account_number, statements
        except ValueError:
            try:
                with zipfile.ZipFile(BytesIO(data_file)) as data:
                    currency = None
                    account_number = None
                    transactions = []
                    for member in data.namelist():
                        currency, account_number, new = self._parse_file(
                            data.open(member).read()
                        )
                        transactions.extend(new)
                return currency, account_number, transactions
            except (zipfile.BadZipFile, ValueError):
                pass
            # Not a camt file, returning super will call next candidate:
            _logger.debug("Statement file was not a camt file.",
                          exc_info=True)
        return super(AccountBankStatementImport, self)._parse_file(data_file)

    @api.model
    def _normalize(self, statements):
        # It is possible that statements (/Document/BkToCstmrStmt/Stmt/Ntry) cover more than one month.
        # This is bad if several months concern more than one period.
        # We check here if all transactions belong to the same month. If this is not the case a new
        # statement is created.

        # Statements with transactions per month (key = date, with the day set to the first
        stats_per_month = {}

        for statement in statements:
            for transaction in statement['transactions']:
                trans_date = datetime.datetime.strptime(transaction['date'], '%Y-%m-%d')
                trans_month = trans_date.replace(day=1)
                if trans_month in stats_per_month:
                    # statement already exists
                    stat = stats_per_month[trans_month]
                else:
                    # no statement yet with this month. Create a new one and copy the data
                    stat = {}
                    stats_per_month[trans_month] = stat
                    stat['name'] = statement['name']
                    stat['transactions'] = []
                    stat['date'] = trans_date
                    stat['balance_start'] = statement['balance_start']
                    if 'currency_code' in statement:
                        stat['currency_code'] = statement['currency_code']
                    if 'account_number' in statement:
                        stat['account_number'] = statement['account_number']
                    if 'attachments' in statement:
                        stat['attachments'] = statement['attachments']

                stat['transactions'].append(transaction)

        # Recalculate the balances. To do this we need a sorted list of statements.
        ordered_stats = collections.OrderedDict(sorted(stats_per_month.items(), key=lambda t: t[0]))
        start = True
        for statement in ordered_stats.values():
            if start:
                current_value = statement['balance_start']
                start = False
            else:
                statement['balance_start'] = current_value
            for transaction in statement['transactions']:
                current_value += transaction['amount']
            statement['balance_end'] = current_value
            statement['balance_end_real'] = current_value

        # return new statements
        return ordered_stats.values()

    def _complete_stmts_vals(self, stmts_vals, journal, account_number):
        stmts_vals = super(AccountBankStatementImport, self)._complete_stmts_vals(stmts_vals, journal, account_number)
        for st_vals in stmts_vals:
            for line_vals in st_vals['transactions']:
                isr = 'ref' in line_vals and line_vals['ref']
                self._search_invoice_and_fill_transaction(isr, line_vals)
        return stmts_vals

    def _search_invoice_and_fill_transaction(self, isr, transaction):
        paymentslip_model = self.env['l10n_ch.payment_slip']
        isr_spaced = isr and paymentslip_model._space(isr)
        invoice = None

        if 'party_type' in transaction and transaction['party_type'] == 'DBIT':
            # in-invoice
            prefix =  _("payout") + ": "

            invoice_model = self.env['account.invoice']

            # start searching in-invoice with EndToEndId
            invoice = self._search_invoice_with_EndToEndId(transaction)

            if not invoice and isr:
                # try searching in-invoice with isr
                _logger.debug("Searching invoice with ISR=%s" % isr)
                invoice = invoice_model.search(['|', ('reference', '=', isr), ('reference', '=', isr_spaced)], limit=1)
                if invoice:
                    _logger.info("Found in-invoice with ISR: %s, invoice.id %s" % (isr, invoice.id))

            if not invoice and 'name' in transaction:
                # try searching in-invoice with in-invoice reference stored in name
                _logger.debug("Searching invoice with reference=%s" % transaction['name'])
                invoice = invoice_model.search([('reference', '=', transaction['name'])], limit=1)
                if invoice:
                    _logger.info("Found in-invoice with reference (name): %s, invoice.id %s" %
                                 (invoice.reference, invoice.id))
        else:
            # out-invoice
            prefix = _("deposit") + ": "
            if isr:
                _logger.debug("Searching out-invoice with ISR=%s" % isr)
                payslip = paymentslip_model.search(['|', ('reference', '=', isr), ('reference', '=', isr_spaced)], limit=1)
                if payslip and payslip.invoice_id:
                    invoice = payslip.invoice_id
                    _logger.info("Payment Slip/invoice found with ISR: %s, invoice.id %s" % (isr, invoice.id))

        if invoice:
            if invoice.reference:
                transaction['ref'] = prefix + invoice.number + ", " + invoice.reference
            else:
                transaction['ref'] = prefix + invoice.number

            if invoice.name:
                transaction['name'] = invoice.name
            else:
                # if there is no name let's take the number
                transaction['name'] = invoice.number

            if invoice.commercial_partner_id:
                transaction['partner_id'] = invoice.commercial_partner_id.get_root_partner_id()
            elif invoice.partner_id:
                transaction['partner_id'] = invoice.partner_id.get_root_partner_id()
            return

        _logger.info("No invoice found with ISR: %s" % isr)

        if 'name' in transaction and 'additional_info' in transaction and transaction['additional_info'].find(transaction['name']) < 0:
            transaction['name'] = transaction['name'] + ", " + transaction['additional_info']
        elif 'additional_info' in transaction:
            transaction['name'] = transaction['additional_info']

    def _search_invoice_with_EndToEndId(self, transaction):
        if 'EndToEndId' in transaction:
            _logger.debug("Searching invoice with EndToEndId=%s" % transaction['EndToEndId'])
            invoice_model = self.env['account.invoice']
            query = " select inv.id"
            query += "  from bank_payment_line bpl"
            query += "  join account_payment_line apl on apl.bank_line_id = bpl.id"
            query += "  join account_move_line aml on aml.id = apl.move_line_id"
            query += "  join account_invoice inv on inv.id = aml.invoice_id"
            query += " where bpl.name = %s"
            self.env.cr.execute(query, (transaction['EndToEndId'],))
            invoice_ids = self.env.cr.fetchone()
            if invoice_ids:
                invoices = invoice_model.browse(invoice_ids)
                _logger.info("Found invoice with EndToEndId: %s, partner_id: %s, EndToEndId: %s" %
                             (invoices[0].number, invoices[0].commercial_partner_id.id, transaction['EndToEndId']))
                return invoices[0]
        _logger.info("No invoice found with EndToEndId")
        return None

class ResPartner(models.Model):
    _inherit = "res.partner"

    # Returns the first company of the partner's hierarchy traversing to the root
    def get_root_partner_id(self):
        if self.is_company:
            return self.id
        if self.parent_id:
            return self.parent_id.get_root_partner_id()
        else:
            return self.id

class AccountBankStatementLine(models.Model):

    _inherit = 'account.bank.statement.line'

    @api.model
    def create(self, vals):
        # remove previously added and only temporary attributes before persisting
        vals.pop('party_type', None)
        vals.pop('additional_info', None)
        vals.pop('EndToEndId', None)
        return super(AccountBankStatementLine, self).create(vals)
