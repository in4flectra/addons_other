# © 2013-2017 Therp BV <http://therp.nl>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    'name': 'CAMT Format Bank Statements Import',
    'version': '1.0',
    'license': 'AGPL-3',
    'author': 'Odoo Community Association (OCA), Therp BV',
    'website': 'https://gitlab.com/flectra-community/bank-statement-import',
    'category': 'Banking addons',
    'depends': [
        'account_bank_statement_import',
        'l10n_ch_payment_slip',
        'account_payment_order',
    ],
    'data': [
        'views/account_bank_statement_import.xml',
    ],
}
