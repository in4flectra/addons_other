{
    "name": "Disable payments in POS",
    "summary": "Control access to the POS payments",
    "version": "7.0",
    "author": "IT-Projects LLC, Ivan Yelizariev",
    "license": "Other OSI approved licence",  # MIT
    "category": "Point of Sale",
    "website": "https://yelizariev.github.io",
    "depends": ["point_of_sale"],
    "data": ["views/views.xml"],
    "installable": True,
}
