/* Copyright 2018 Onestein
 * License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl). */

flectra.define('website_theme_flexible.frontend', function(require) {
"use strict";

    var base = require('web_editor.base');

    base.ready().then(function () {
        var navbar = $('header > .navbar');

        if (navbar.attr('data-do-stick') === '1') {
            //TODO: Add nice animation
            navbar.addClass('navbar-fixed-top');
            $('main').addClass('main-fixed-top');
            resize_navbar();
        }
    });

    $(window).on('resize', function(event) {
        resize_navbar();
    });

    var resize_navbar = function() {
        var navbar = document.getElementsByClassName('navbar-fixed-top');
        var main = document.getElementsByClassName('main-fixed-top');
        if (navbar.length > 0 && main.length > 0) {
            var div = $('.main-fixed-top > div');
            if (div.length > 0) {
                div[0].style.paddingTop = navbar[0].offsetHeight.toString() + 'px';
            }
        }
    }

});
