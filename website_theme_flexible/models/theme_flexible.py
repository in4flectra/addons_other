# Copyright 2018 Onestein
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from flectra import api, fields, models


class ThemeFlexible(models.Model):

    _name = 'theme.flexible'

    name = fields.Char(name='Name')

    layout = fields.Selection(
        selection=[
            ('full_width', 'Full Width'),
            ('boxed', 'Boxed'),
            ('postcard', 'Postcard')
        ],
        string='Layout',
        default='full_width'
    )
    snippet_border_radius = fields.Integer(string='Snippet Border Radius', default=0)

    color_alpha = fields.Char(string='Color Alpha', default='#1CC1A9')
    color_beta = fields.Char(string='Color Beta', default='#875A7B')
    color_gamma = fields.Char(string='Color Gamma', default='#BA3C3D')
    color_delta = fields.Char(string='Color Delta', default='#0D6759')
    color_epsilon = fields.Char(string='Color Epsilon', default='#0B2E59')

    @classmethod
    def _add_shades(cls, color):
        setattr(cls, 'amount_%s_lighter' % color, fields.Integer(
            default=10
        ))

        setattr(cls, 'amount_%s_light' % color, fields.Integer(
            default=5
        ))

        setattr(cls, 'amount_%s_dark' % color, fields.Integer(
            default=5
        ))

        setattr(cls, 'amount_%s_darker' % color, fields.Integer(
            default=10
        ))

    @classmethod
    def _add_font(cls, name):
        setattr(cls, 'font_%s' % name, fields.Char(
            default='Arial'
        ))
        setattr(cls, 'font_%s_google' % name, fields.Boolean())
        setattr(cls, 'font_%s_weight' % name, fields.Integer(
            default=400
        ))
        setattr(cls, 'font_%s_italic' % name, fields.Boolean())
        setattr(cls, 'font_%s_underline' % name, fields.Boolean())

    google_query = fields.Char(compute='_compute_google_query')

    @api.multi
    @api.depends(
        'font_normal', 'font_normal_weight', 'font_normal_italic',
        'font_normal_google',
        'font_code', 'font_code_weight', 'font_code_italic',
        'font_code_google',
        'font_header_1', 'font_header_1_weight', 'font_header_1_italic',
        'font_header_1_google',
        'font_header_2', 'font_header_2_weight', 'font_header_2_italic',
        'font_header_2_google',
        'font_header_3', 'font_header_3_weight', 'font_header_3_italic',
        'font_header_3_google',
        'font_header_4', 'font_header_4_weight', 'font_header_4_italic',
        'font_header_4_google',
        'font_header_5', 'font_header_5_weight', 'font_header_5_italic',
        'font_header_5_google',
        'font_header_6', 'font_header_6_weight', 'font_header_6_italic',
        'font_header_6_google',
        'font_menu', 'font_menu_weight', 'font_menu_italic',
        'font_menu_google'
    )
    def _compute_google_query(self):
        styles = ['normal', 'code', 'header_1', 'header_2',
                  'header_3', 'header_4', 'header_5', 'header_6',
                  'menu']
        for theme in self:
            fonts = {}
            for style in styles:
                if theme['font_%s_google' % style]:
                    if theme['font_%s' % style] not in fonts:
                        fonts[theme['font_%s' % style]] = []
                    g_str = ''
                    g_str += str(theme['font_%s_weight' % style])
                    if theme['font_%s_italic' % style]:
                        g_str += 'i'
                    if g_str not in fonts[theme['font_%s' % style]]:
                        fonts[theme['font_%s' % style]].append(g_str)
            google_query = ''
            for font in fonts:
                google_query += font.replace(' ', '+') + ':'
                for variant in fonts[font]:
                    google_query += variant + ','
                google_query = google_query[:-1] + '|'
            google_query = google_query[:-1]
            theme.google_query = google_query

    bg_color = fields.Char(
        default='#FFFFFF'
    )
    footer_bg_color = fields.Char(
        default='#F8F8F8'
    )

    menu_logo = fields.Selection(
        selection=[
            ('none', 'None'),
            ('left_text', 'Left / Name'),
            ('right_text', 'Right / Name'),
            ('left_logo', 'Left / Logo'),
            ('right_logo', 'Right / Logo')
        ],
        default='left_logo'
    )
    menu_alignment = fields.Selection(
        selection=[
            ('left', 'Left'),
            ('right', 'Right'),
            ('center', 'Center')
        ],
        default='right'
    )
    menu_sticky = fields.Boolean(string='Menu Sticky')
    menu_color = fields.Char(string='Menu Font Color', default='#333333')
    menu_bg = fields.Char(string='Menu Background Color', default='#f8f8f8')

    breadcrumb_color = fields.Char(string='Breadcrumb Font Color', default='#ccc')
    breadcrumb_background_color = fields.Char(string='Breadcrumb Background Color', default='#f5f5f5')
    breadcrumb_active_color = fields.Char(string='Breadcrumb Active Font Color', default='#777')

    dropdown_menu_background_color = fields.Char(string='Dropdown Menu Background Color', default='#fff')
    dropdown_menu_border_color = fields.Char(string='Dropdown Menu Border Color', default='#e5e5e5')
    dropdown_menu_divider_background_color = fields.Char(string='Dropdown Menu Divider Background Color', default='#e5e5e5')
    dropdown_menu_link_color = fields.Char(string='Dropdown Menu Link Font Color', default='#333')
    dropdown_menu_active_link_color = fields.Char(string='Dropdown Menu Active Link Font Color', default='#fff')
    dropdown_menu_disabled_link_color = fields.Char(string='Dropdown Menu Disabled Link Font Color', default='#777')

    nav_list_link_background_color = fields.Char(string='Navigator Link Background Color', default='#eee')
    nav_list_disabled_link_color = fields.Char(string='Navigator Disabled Link Font Color', default='#777')
    nav_list_disabled_link_background_color = fields.Char(string='Navigator Disabled Link Background Color', default='transparent')
    nav_open_link_background_color = fields.Char(string='Navigator Open Link Background Color', default='#eee')
    nav_open_link_border_color = fields.Char(string='Navigator Open Link Border Color', default='#eee')
    nav_divider_background_color = fields.Char(string='Navigator Divider Background Color', default='#e5e5e5')
    nav_tabs_border_bottom_color = fields.Char(string='Navigator Tabs Border Bottom Color', default='#ddd')
    nav_tabs_list_link_border_color = fields.Char(string='Navigator Tabs Link Border Color', default='transparent')
    nav_tabs_list_link_hover_border_color = fields.Char(string='Navigator Tabs Link Hover Border Color', default='#ddd')
    nav_tabs_list_active_link_background_color = fields.Char(string='Navigator Tabs Active Link Background Color', default='#fff')
    nav_tabs_list_active_link_border_color = fields.Char(string='Navigator Tabs Active Link Border Color', default='#ddd')
    nav_tabs_list_active_link_border_bottom_color = fields.Char(string='Navigator Tabs Active Link Border Bottom Color', default='transparent')
    nav_tabs_justified_active_link_border_color = fields.Char(string='Navigator Tabs Justified Active Link Border Color', default='#ddd')
    nav_pills_list_active_link_color = fields.Char(string='Navigator Pills Active Link Font Color', default='#fff')
    nav_pills_list_active_link_background_color = fields.Char(string='Navigator Pills Active Link Background Color', default='#337ab7')
    navbar_border_color = fields.Char(string='Navigator Bar Border Color', default='transparent')
    navbar_collapse_border_top_color = fields.Char(string='Navigator Bar Collapse Border Top Color', default='transparent')

    anchor_color = fields.Char(string='Link Font Color', default='#337ab7')
    anchor_background_color = fields.Char(string='Link Background Color', default='transparent')
    anchor_active_color = fields.Char(string='Link Active Font Color', default='#fff')
    anchor_active_background_color = fields.Char(string='Link Active Background Color', default='#337ab7')
    anchor_footer_color = fields.Char(string='Link Footer Font Color', default='#337ab7')
    anchor_footer_background_color = fields.Char(string='Link Footer Background Color', default='transparent')

    label_default_background_color = fields.Char(string='Default Label Background Color', default='#fff')
    label_primary_background_color = fields.Char(string='Primary Label Background Color', default='#337ab7')
    label_success_background_color = fields.Char(string='Success Label Background Color', default='#5cb85c')
    label_info_background_color = fields.Char(string='Info Label Background Color', default='#5bc0de')
    label_warning_background_color = fields.Char(string='Warning Label Background Color', default='#f0ad4e')
    label_danger_background_color = fields.Char(string='Danger Label Background Color', default='#d9534f')

    alert_success_color = fields.Char(string='Success Alert Font Color', default='#3c763d')
    alert_success_background_color = fields.Char(string='Success Alert Background Color', default='#dff0d8')
    alert_success_border_color = fields.Char(string='Success Alert Border Color', default='#d6e9c6')
    alert_success_hr_border_top_color = fields.Char(string='Success Alert HR Top Border Color', default='#c9e2b3')
    alert_success_alert_link_color = fields.Char(string='Success Alert Link Color', default='#2b542c')

    alert_info_color = fields.Char(string='Info Alert Font Color', default='#31708f')
    alert_info_background_color = fields.Char(string='Info Alert Background Color', default='#d9edf7')
    alert_info_border_color = fields.Char(string='Info Alert Border Color', default='#bce8f1')
    alert_info_hr_border_top_color = fields.Char(string='Info Alert HR Top Border Color', default='#a6e1ec')
    alert_info_alert_link_color = fields.Char(string='Info Alert Link Color', default='#245269')

    alert_warning_color = fields.Char(string='Warning Alert Font Color', default='#8a6d3b')
    alert_warning_background_color = fields.Char(string='Warning Alert Background Color', default='#fcf8e3')
    alert_warning_border_color = fields.Char(string='Warning Alert Border Color', default='#faebcc')
    alert_warning_hr_border_top_color = fields.Char(string='Warning Alert HR Top Border Color', default='#f7e1b5')
    alert_warning_alert_link_color = fields.Char(string='Warning Alert Link Color', default='#66512c')

    alert_danger_color = fields.Char(string='Danger Alert Font Color', default='#a94442')
    alert_danger_background_color = fields.Char(string='Danger Alert Background Color', default='#f2dede')
    alert_danger_border_color = fields.Char(string='Danger Alert Border Color', default='#ebccd1')
    alert_danger_hr_border_top_color = fields.Char(string='Danger Alert HR Top Border Color', default='#e4b9c0')
    alert_danger_alert_link_color = fields.Char(string='Danger Alert Link Color', default='#843534')

    btn_default_color = fields.Char(string='Default Button Font Color', default='#333333')
    btn_default_background_color = fields.Char(string='Default Button Background Color', default='#fff')
    btn_default_border_color = fields.Char(string='Default Button Border Color', default='#cccccc')

    btn_primary_color = fields.Char(string='Primary Button Font Color', default='#fff')
    btn_primary_background_color = fields.Char(string='Primary Button Background Color', default='#337ab7')
    btn_primary_border_color = fields.Char(string='Primary Button Border Color', default='#2e6da4')

    btn_success_color = fields.Char(string='Success Button Font Color', default='#fff')
    btn_success_background_color = fields.Char(string='Success Button Background Color', default='#5cb85c')
    btn_success_border_color = fields.Char(string='Success Button Border Color', default='#4cae4c')

    btn_info_color = fields.Char(string='Info Button Font Color', default='#fff')
    btn_info_background_color = fields.Char(string='Info Button Background Color', default='#5bc0de')
    btn_info_border_color = fields.Char(string='Info Button Border Color', default='#46b8da')

    btn_warning_color = fields.Char(string='Warning Button Font Color', default='#fff')
    btn_warning_background_color = fields.Char(string='Warning Button Background Color', default='#f0ad4e')
    btn_warning_border_color = fields.Char(string='Warning Button Border Color', default='#eea236')

    btn_danger_color = fields.Char(string='Danger Button Font Color', default='#fff')
    btn_danger_background_color = fields.Char(string='Danger Button Background Color', default='#d9534f')
    btn_danger_border_color = fields.Char(string='Danger Button Border Color', default='#d43f3a')

    panel_default_border_color = fields.Char(string='Default Panel Border Color', default='#ddd')
    panel_default_heading_color = fields.Char(string='Default Panel Heading Font Color', default='#333')
    panel_default_heading_background_color = fields.Char(string='Default Panel Heading Background Color', default='#f5f5f5')
    panel_default_heading_border_color = fields.Char(string='Default Panel Heading Border Color', default='#ddd')
    panel_default_heading_border_top_color = fields.Char(string='Default Panel Heading Top Border Color', default='#ddd')
    panel_default_heading_badge_color = fields.Char(string='Default Panel Heading Badge Font Color', default='#f5f5f5')
    panel_default_heading_badge_background_color = fields.Char(string='Default Panel Heading Badge Background Color', default='#333')
    panel_default_footer_border_bottom_color = fields.Char(string='Default Panel Footer Bottom Border Color', default='#ddd')

    panel_primary_border_color = fields.Char(string='Primary Panel Border Color', default='#337ab7')
    panel_primary_heading_color = fields.Char(string='Primary Panel Heading Font Color', default='#fff')
    panel_primary_heading_background_color = fields.Char(string='Primary Panel Heading Background Color', default='#337ab7')
    panel_primary_heading_border_color = fields.Char(string='Primary Panel Heading Border Color', default='#337ab7')
    panel_primary_heading_border_top_color = fields.Char(string='Primary Panel Heading Top Border Color', default='#337ab7')
    panel_primary_heading_badge_color = fields.Char(string='Primary Panel Heading Badge Font Color', default='#337ab7')
    panel_primary_heading_badge_background_color = fields.Char(string='Primary Panel Heading Badge Background Color', default='#fff')
    panel_primary_footer_border_bottom_color = fields.Char(string='Primary Panel Footer Bottom Border Color', default='#337ab7')

    panel_success_border_color = fields.Char(string='Success Panel Border Color', default='#d6e9c6')
    panel_success_heading_color = fields.Char(string='Success Panel Heading Font Color', default='#3c763d')
    panel_success_heading_background_color = fields.Char(string='Success Panel Heading Background Color', default='#dff0d8')
    panel_success_heading_border_color = fields.Char(string='Success Panel Heading Border Color', default='#d6e9c6')
    panel_success_heading_border_top_color = fields.Char(string='Success Panel Heading Top Border Color', default='#d6e9c6')
    panel_success_heading_badge_color = fields.Char(string='Success Panel Heading Badge Font Color', default='#dff0d8')
    panel_success_heading_badge_background_color = fields.Char(string='Success Panel Heading Badge Background Color', default='#3c763d')
    panel_success_footer_border_bottom_color = fields.Char(string='Success Panel Footer Bottom Border Color', default='#d6e9c6')

    panel_info_border_color = fields.Char(string='Info Panel Border Color', default='#bce8f1')
    panel_info_heading_color = fields.Char(string='Info Panel Heading Font Color', default='#31708f')
    panel_info_heading_background_color = fields.Char(string='Info Panel Heading Background Color', default='#d9edf7')
    panel_info_heading_border_color = fields.Char(string='Info Panel Heading Border Color', default='#bce8f1')
    panel_info_heading_border_top_color = fields.Char(string='Info Panel Heading Top Border Color', default='#bce8f1')
    panel_info_heading_badge_color = fields.Char(string='Info Panel Heading Badge Font Color', default='#d9edf7')
    panel_info_heading_badge_background_color = fields.Char(string='Info Panel Heading Badge Background Color', default='#31708f')
    panel_info_footer_border_bottom_color = fields.Char(string='Info Panel Footer Bottom Border Color', default='#bce8f1')

    panel_warning_border_color = fields.Char(string='Warning Panel Border Color', default='#faebcc')
    panel_warning_heading_color = fields.Char(string='Warning Panel Heading Font Color', default='#8a6d3b')
    panel_warning_heading_background_color = fields.Char(string='Warning Panel Heading Background Color', default='#fcf8e3')
    panel_warning_heading_border_color = fields.Char(string='Warning Panel Heading Border Color', default='#faebcc')
    panel_warning_heading_border_top_color = fields.Char(string='Warning Panel Heading Top Border Color', default='#faebcc')
    panel_warning_heading_badge_color = fields.Char(string='Warning Panel Heading Badge Font Color', default='#fcf8e3')
    panel_warning_heading_badge_background_color = fields.Char(string='Warning Panel Heading Badge Background Color', default='#8a6d3b')
    panel_warning_footer_border_bottom_color = fields.Char(string='Warning Panel Footer Bottom Border Color', default='#faebcc')

    panel_danger_border_color = fields.Char(string='Danger Panel Border Color', default='#ebccd1')
    panel_danger_heading_color = fields.Char(string='Danger Panel Heading Font Color', default='#a94442')
    panel_danger_heading_background_color = fields.Char(string='Danger Panel Heading Background Color', default='#f2dede')
    panel_danger_heading_border_color = fields.Char(string='Danger Panel Heading Border Color', default='#ebccd1')
    panel_danger_heading_border_top_color = fields.Char(string='Danger Panel Heading Top Border Color', default='#ebccd1')
    panel_danger_heading_badge_color = fields.Char(string='Danger Panel Heading Badge Font Color', default='#f2dede')
    panel_danger_heading_badge_background_color = fields.Char(string='Danger Panel Heading Badge Background Color', default='#a94442')
    panel_danger_footer_border_bottom_color = fields.Char(string='Danger Panel Footer Bottom Border Color', default='#ebccd1')

    css = fields.Text()


ThemeFlexible._add_shades('alpha')
ThemeFlexible._add_shades('beta')
ThemeFlexible._add_shades('gamma')
ThemeFlexible._add_shades('delta')
ThemeFlexible._add_shades('epsilon')

ThemeFlexible._add_font('normal')
ThemeFlexible._add_font('code')
ThemeFlexible._add_font('header_1')
ThemeFlexible._add_font('header_2')
ThemeFlexible._add_font('header_3')
ThemeFlexible._add_font('header_4')
ThemeFlexible._add_font('header_5')
ThemeFlexible._add_font('header_6')
ThemeFlexible._add_font('menu')
