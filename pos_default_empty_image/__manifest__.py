# © 2015 Akretion, GRAP, OCA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    'name': 'POS Default empty image',
    'version': '0.0',
    'category': 'Point of Sale',
    'summary': 'Optimize loading time for products without image',
    'author': "Akretion, GRAP, Odoo Community Association (OCA)",
    'website': "https://www.github.com/OCA/pos",
    'license': 'AGPL-3',
    'depends': [
        'point_of_sale',
    ],
    'data': [
        'views/assets.xml',
        'views/pos_config.xml',
    ],
    'qweb': [
        'static/src/xml/pos_default_empty_image.xml',
    ],
    'installable': True,
}
