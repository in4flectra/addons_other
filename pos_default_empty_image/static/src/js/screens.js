flectra.define('pos_default_empty_image.screens', function (require) {
    'use strict';

    var core = require('web.core');
    var screens = require('point_of_sale.screens');

    var QWeb = core.qweb;

    var ProductListImageWidget = screens.ProductListWidget.include({

        get_product_image_url: function(product) {
            if (this.pos.config.iface_show_product_images && product.has_image) {
                return this._super(product);
            }
        },

        render_product: function(product) {
            if (this.pos.config.iface_show_product_images && product.has_image) {
                return this._super(product);
            } else {
                var cached = this.product_cache.get_node(product.id);
                if (!cached) {
                    var current_pricelist = this._get_active_pricelist();
                    var product_html = QWeb.render('ProductNoImage', {
                        widget: this,
                        product: product,
                        pricelist: current_pricelist,
                    });
                    var product_node = document.createElement('div');
                    product_node.innerHTML = product_html;
                    product_node = product_node.childNodes[1];
                    this.product_cache.cache_node(product.id,product_node);
                    return product_node;
                }
                return cached;
            }
        },
    });

});
