# -*- coding: utf-8 -*-

from flectra import api, fields, models


class POSDefaultEmptyImageProductProduct(models.Model):

    _inherit = 'product.product'

    @api.depends('image', 'product_tmpl_id.image')
    def _compute_has_image(self):
        for product in self:
            if product.image or product.product_tmpl_id.image:
                product.has_image = True
            else:
                product.has_image = False

    has_image = fields.Boolean(string='Has Image', compute=_compute_has_image, store=True)
