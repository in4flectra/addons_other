# -*- coding: utf-8 -*-

from flectra import fields, models


class POSDefaultEmptyImagePOSConfig(models.Model):

    _inherit = 'pos.config'

    iface_show_product_images = fields.Boolean(string='Display Images', default=True, help='Display the images of the products')
